
FROM nginx:1.21.4-alpine

COPY ./build /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]
