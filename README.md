# TEST REACT and Pipeline de CI/CD Completo

El objetivo de este proyecto es obtener una demo de un pipeline de **CI/CD** completo aplicando **DevSecOps**.

#### **Teconologias:**
-  React
-  Material UI
-  Gitlab CI
-  Argo CD
-  Kustomize
-  Kubernetes (k3s cluster)

APP MANIFEST: https://gitlab.com/augustodelg/app-manifest 
LINK DEMO: https://prod.demo.augustodelgrosso.com.ar/ 
